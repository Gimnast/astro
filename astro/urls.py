from django.contrib import admin
from django.urls import path
from main import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.AjaxHandlerView.as_view(), name="index"),
    path('article_show', views.article_show, name="article_show"),
    path('in', views.in_page, name="in"),
    path('next_stage', views.next_stage, name="next_stage")
]
