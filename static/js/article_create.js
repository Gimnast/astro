let x;
let y;
let interval_for_add_text;
let interval_for_add_image;
let interval_for_text;
let interval_for_scale;
let text_moves_id = "";
let text_scale_id = "";
let can_blank_touch = false;
let count_of_images = 0;
let count_of_texts = 0;
let input_to_blank;
var pressedKeys = {}; 
var send_JSON = {};


$(document).mousemove(function(event){
    x = event.pageX;
    y = event.pageY;
});

window.onload = function(){

    input_to_blank = document.getElementById("input_to_blank");

    let add_text = document.getElementById("add_text");
    let add_image = document.getElementById("add_image");
    let blank = document.getElementById("blank");
    let save_btn = document.getElementById("save_btn");
    save_btn.onclick = update_JSON_data;

    let get_data = document.getElementById("get_data").value;
    var getted_JSON = JSON.parse(get_data);
    for(key in getted_JSON){
        if(key == "blank"){
            blank.style.height = getted_JSON[key]["height"];
        }
        if(getted_JSON[key]["type"] == "TEXTAREA"){
            let el = document.createElement("textarea");
            el.style.marginLeft = getted_JSON[key]["marginLeft"];
            el.style.marginTop = getted_JSON[key]["marginTop"];
            el.style.fontSize = getted_JSON[key]["font-size"];
            count_of_texts += 1;
            el.id = "text_" + count_of_texts;
            
            let true_value = "";
            for(i in getted_JSON[key]["value"]){
                if(getted_JSON[key]["value"][i] != '~'){
                    true_value += getted_JSON[key]["value"][i];
                }
                else{
                    true_value += " ";
                }
            }

            el.value = true_value;
            el.style.position = "absolute";
            el.classList.add("added_text");
            el.style.width = getted_JSON[key]["text_width"];
            el.style.height = getted_JSON[key]["text_height"];
            blank.appendChild(el);
        }
        if(getted_JSON[key]["type"] == "IMG"){
            let el = document.createElement("img");
            el.style.marginLeft = getted_JSON[key]["marginLeft"];
            el.style.marginTop = getted_JSON[key]["marginTop"];
            el.src = getted_JSON[key]["src"];
            el.width = getted_JSON[key]["width"];
            el.height = getted_JSON[key]["height"];
            count_of_images += 1;
            el.id = "image_" + count_of_images;
            el.style.position = "absolute";
            blank.appendChild(el);
        }
    }

    add_image.onclick = add_image_onclick;
    add_text.onclick = add_text_onclick;
    blank.onclick = blank_onclick;

    setInterval(texts_event_checking, 500);
    setInterval(images_event_checking, 500);

    onkeydown = function(e) {
        if(pressedKeys[e.code]) return;
        pressedKeys[e.code] = true;
        if(pressedKeys["AltLeft"] && pressedKeys["Equal"]){
            let element = document.querySelector('textarea:focus');
            if(element != null){
                let fs = element.style.fontSize;
                fs = fs.slice(0, fs.length - 2) - 0 + 4;
                if(fs <= 200){
                    element.style.fontSize = fs + "px";
                }   
            }
        }
        if(pressedKeys["AltLeft"] && pressedKeys["Minus"]){
            let element = document.querySelector('textarea:focus');
            if(element != null){
                let fs = element.style.fontSize;
                fs = fs.slice(0, fs.length - 2) - 0 - 4;
                if(fs >= 12){
                    element.style.fontSize = fs + "px";
                }
            }
        }
        if(pressedKeys["Delete"]){
            let element = document.querySelector('textarea:focus');
            if(element != null){
                blank.removeChild(element);
            }
        }
    }


    onkeyup = function(e) {
        delete pressedKeys[e.code];
    }
}


function add_image_onclick(){
    var list = document.getElementById('blank');
	var li = document.createElement('button');
    li.className = "add_text";
    li.innerHTML = "image";
    li.id = "new_btn_image";
	list.appendChild(li);
    interval_for_add_image = setInterval(Setter_add_image, 10);
}

function add_text_onclick(){
    var list = document.getElementById('blank');
	var li = document.createElement('button');
    li.className = "add_text";
    li.innerHTML = "text";
    li.id = "new_btn_text";
	list.appendChild(li);
    interval_for_add_text = setInterval(Setter_add_text, 10);
}

function Setter_add_text(){
    var list = document.getElementById('blank');
    var new_btn = document.getElementById('new_btn_text');
    let state = "position: fixed; z-index: 3; ";
    let true_y = y - $(list).offset().top - 10 - window.pageYOffset;
    let true_x = x - $(list).offset().left - 20 - window.pageXOffset;
    let margins = "margin-left: " + true_x + "px;" + "margin-top: " + true_y + "px;";
    new_btn.style = state + margins + "color: white";
}

function Setter_add_image(){
    var list = document.getElementById('blank');
    var new_btn = document.getElementById('new_btn_image');
    let state = "position: fixed; z-index: 3; ";
    let true_y = y - $(list).offset().top - 10 - window.pageYOffset;
    let true_x = x - $(list).offset().left - 20 - window.pageXOffset;
    let margins = "margin-left: " + true_x + "px;" + "margin-top: " + true_y + "px;";
    new_btn.style = state + margins + "color: white";
}

function blank_onclick(){
    if(x < $(this).offset().left + 1 || y < $(this).offset().top + 1) { return; }
    if(text_moves_id != "" && can_blank_touch){
        if(x > $(this).offset().left + this.offsetWidth || y > $(this).offset().top + this.offsetHeight){
            return;
        }
        clearInterval(interval_for_text);
        text_moves_id = "";
        can_blank_touch = false;
    }
    if(text_scale_id != "" && can_blank_touch){
        if(x > $(this).offset().left + this.offsetWidth + 1 || y > $(this).offset().top + this.offsetHeight + 1){
            return;
        }
        var el = document.getElementById(text_scale_id);
        el.naturalWidth = el.style.width + "px";
        el.naturalHeight = el.style.height + "px";
        clearInterval(interval_for_scale);
        text_scale_id = "";
        can_blank_touch = false;
    }
    var new_btn_text = document.getElementById('new_btn_text');
    var new_btn_image = document.getElementById('new_btn_image');
    if(new_btn_text == null && new_btn_image == null){ return; }
    clearInterval(interval_for_add_text);
    clearInterval(interval_for_add_image);
    var list = document.getElementById('blank');
    if(new_btn_text != null){
        count_of_texts += 1;
        new_btn_text.remove();
        let true_y = y - $(list).offset().top - 10;
        let true_x = x - $(list).offset().left - 20;
        let margins = "margin-left: " + true_x + "px;" + "margin-top: " + true_y + "px;";
        var li = document.createElement('textarea');
        li.style = "position: absolute;" + margins;
        li.classList.add("added_text");
        li.innerHTML = "text";
        li.id = "text_" + count_of_texts;
        li.style.fontSize = 20 + "px";
        list.appendChild(li);
    }
    else{
        count_of_images += 1;
        new_btn_image.remove();
        let true_y = y - $(list).offset().top - 10;
        let true_x = x - $(list).offset().left - 20;
        let margins = "margin-left: " + true_x + "px;" + "margin-top: " + true_y + "px;";
        var li = document.createElement('input');
        li.type = "file";
        li.style = "position: absolute;" + margins;
        li.innerHTML = "Upload Image";
        li.id = "image_" + count_of_images;
        list.appendChild(li);
        $("#image_" + count_of_images).change(function() {
            readURL(this, "image_" + count_of_images);
        });
    }
}

function texts_event_checking(){
    for(let i = 0; i <= count_of_texts; ++i){
        var el = document.getElementById("text_" + i);
        if(el != null){
            var list = document.getElementById('blank');
            if(el.offsetWidth + $(el).offset().left > list.offsetWidth + $(list).offset().left){
                el.style.width = list.offsetWidth + $(list).offset().left - $(el).offset().left - 5 + "px";
            }
            if(el.offsetHeight + $(el).offset().top > list.offsetHeight + $(list).offset().top){
                el.style.height = list.offsetHeight + $(list).offset().top - $(el).offset().top - 5 + "px";
            }
            el.onclick = texts_event_doing;
            el.onmousemove = texts_movements;
        }
    }
}

function texts_movements(){
    let true_y = y - $(this).offset().top;
    let true_x = x - $(this).offset().left;
    if(((true_x >= 0 && true_x <= 3) || (true_y >= 0 && true_y <= 3)) && document.querySelector('textarea:focus')){
        this.style.cursor = "move";
    }
    else{
        this.style.cursor = "auto";
    }
}

function texts_event_doing(){
    if(this.style.cursor == "move" && text_moves_id == ""){
        text_moves_id = this.id;
        interval_for_text = setInterval(text_movement, 10);
    }
}

function text_movement(){
    var list = document.getElementById('blank');
    var el = document.getElementById(text_moves_id);
    let true_y = y - $(list).offset().top - 3;
    let true_x = x - $(list).offset().left - 20;
    el.style.marginTop = true_y + "px";
    el.style.marginLeft = true_x + "px";
    can_blank_touch = true;
}

function readURL(input, this_id) {
    var list = document.getElementById("blank");
    var el = document.getElementById(this_id);
    var new_el = document.createElement('img');
    new_el.id = this_id;
    let ml = el.style.marginLeft;
    let mt = el.style.marginTop;
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            new_el.src = e.target.result;
            new_el.style = "width: " + this.width + "px; " + "height: " + this.height + "px;";
            new_el.style.position = "absolute";
            new_el.style.marginLeft = ml;
            new_el.style.marginTop = mt;
            list.appendChild(new_el);
        }

        reader.readAsDataURL(input.files[0]);
    }
    list.removeChild(el);
}

function images_event_checking(){   
    for(let i = 0; i <= count_of_images; ++i){
        var el = document.getElementById("image_" + i);
        if(el != null){
            if(el.tagName == "IMG" && text_scale_id == "" && text_moves_id == ""){
                var list = document.getElementById('blank');
                if(el.style.width > 955){
                    el.style.width = 955 + "px";
                    el.style.height = ((955 * el.naturalHeight) / el.naturalWidth) + "px"; 
                }
                if(el.offsetWidth + $(el).offset().left > list.offsetWidth + $(list).offset().left + 1){
                    el.style.width = list.offsetWidth + $(list).offset().left - $(el).offset().left - 5 + "px";
                }
                if(el.offsetHeight + $(el).offset().top > list.offsetHeight + $(list).offset().top + 1){
                    el.style.height = list.offsetHeight + $(list).offset().top - $(el).offset().top - 5 + "px";
                }
                el.onclick = images_event_doing;
                el.onmousemove = images_movements;
            }
        }
    }
}

function images_movements(){
    let true_y = y - $(this).offset().top;
    let true_x = x - $(this).offset().left;
    if(((true_x >= 0 && true_x <= 3) || (true_y >= 0 && true_y <= 3))){
        this.style.cursor = "move";
    }
    else if(true_x - this.offsetWidth <= 0 && true_x - this.offsetWidth >= -4 
        && true_y - this.offsetHeight <= 0 && true_y - this.offsetHeight >= -4){
        this.style.cursor = "col-resize";
    }
    else{
        this.style.cursor = "auto";
    }
}

function images_event_doing(){
    if(pressedKeys["Delete"]){
        var list = document.getElementById("blank");
        list.removeChild(this);
    }
    if(this.style.cursor == "move" && text_moves_id == ""){
        text_moves_id = this.id;
        interval_for_text = setInterval(text_movement, 10);
    }
    else if(this.style.cursor == "col-resize" && text_scale_id == ""){
        text_scale_id = this.id;
        interval_for_scale = setInterval(image_movement, 10);
    }
}

function image_movement(){
    var list = document.getElementById('blank');
    var el = document.getElementById(text_scale_id);
    let true_y = y - $(el).offset().top;
    let true_x = x - $(el).offset().left;
    el.style.width = true_x + 2 + "px";
    el.style.height = true_y + 2 + "px";
    can_blank_touch = true;
}

function update_JSON_data(){
    blank = document.getElementById("blank");
    send_JSON["blank"] = {
        "height" : blank.style.height
    };
    for(let ind = 0; ind < blank.childNodes.length; ind++){
        let el = blank.childNodes[ind];
        if(el.tagName == "TEXTAREA" || el.tagName == "IMG"){
            let i = document.getElementById(el.id);
            let value_parsed = "";
            if(i.tagName == "TEXTAREA"){
                for(k in i.value){
                    if(i.value[k] != " "){
                        value_parsed += i.value[k];
                    }
                    else{
                        value_parsed += '~';
                    }
                }
            }
            send_JSON[i.id] = {
                "type": i.tagName,
                "class": i.name,
                "marginLeft": i.style.marginLeft,
                "marginTop": i.style.marginTop,
                "value": value_parsed,
                "font-size": i.style.fontSize,
                "height": i.height,
                "width": i.width,
                "text_width": i.style.width,
                "text_height": i.style.height,
                "src": i.src
            };
        }
    }
    input_to_blank.value = JSON.stringify(send_JSON);
}