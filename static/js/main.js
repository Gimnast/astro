let amount_of_articles = 1;
let count_of_images = 0;
let count_of_texts = 0;

window.onload = function(){
    let update_btn = document.getElementById("update_btn");
    update_btn.onclick = update_btn_click;
    update_btn.click();
}

function update_btn_click(){
    $.ajax({
        url: '',
        type: 'get',
        data: {
            first_id: amount_of_articles,
            last_id: amount_of_articles + 4,
        },
        success: function(response){
            let main = document.getElementById("main");
            for(ind in response){
                let fs = main.style.height;
                fs = fs.slice(0, fs.length - 2) - 0 + 250 + "px";
                main.style.height = fs;
                let data = JSON.parse(response[ind]["title"]);
                let blank = document.createElement('div');
                blank.id = ind;
                blank.classList.add("small_icon");
                blank.marginTop = "30px";
                for(key in data){
                    if(data[key]["type"] == "TEXTAREA"){
                        let el = document.createElement("textarea");
                        el.style.marginLeft = data[key]["marginLeft"];
                        el.style.marginTop = data[key]["marginTop"];
                        el.style.fontSize = data[key]["font-size"];
                        count_of_texts += 1;
                        el.id = "text_" + count_of_texts;
                        
                        let true_value = "";
                        for(i in data[key]["value"]){
                            if(data[key]["value"][i] != '~'){
                                true_value += data[key]["value"][i];
                            }
                            else{
                                true_value += " ";
                            }
                        }
            
                        el.value = true_value;
                        el.style.position = "absolute";
                        el.classList.add("added_text");
                        el.style.width = data[key]["text_width"];
                        el.style.height = data[key]["text_height"];
                        blank.appendChild(el);
                    }
                    if(data[key]["type"] == "IMG"){
                        let el = document.createElement("img");
                        el.style.marginLeft = data[key]["marginLeft"];
                        el.style.marginTop = data[key]["marginTop"];
                        el.src = data[key]["src"];
                        el.width = data[key]["width"];
                        el.height = data[key]["height"];
                        count_of_images += 1;
                        el.id = "image_" + count_of_images;
                        el.style.position = "absolute";
                        blank.appendChild(el);
                    }
                }
                main.appendChild(blank);
            }
            amount_of_articles += 5;
        }
    });
}