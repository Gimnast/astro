# Generated by Django 4.0.3 on 2022-08-26 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='articlefild',
            old_name='user_id',
            new_name='user_created_id',
        ),
        migrations.AddField(
            model_name='articlefild',
            name='preview_title',
            field=models.CharField(default=' ', max_length=9999999999),
            preserve_default=False,
        ),
    ]
