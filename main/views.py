from webbrowser import get
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import logout, login
from django.views.generic import View
from django.shortcuts import HttpResponse
from django.http import JsonResponse
from . models import ArticleFild

import json

def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

class AjaxHandlerView(View):
    def get(self, request):
        first_id = request.GET.get('first_id')
        last_id = request.GET.get('last_id')
        if(type(first_id) != type("123") or type(last_id) != type("123")):
             return render(request, 'index.html')

        first_id, last_id = int(first_id), int(last_id)
        
        sendJson = {}

        for i in range(first_id, last_id + 1):
            try:
                article = ArticleFild.objects.get(id=i)
                sendJson[i] = {}
                sendJson[i]['title'] = article.preview_title
            except: pass


        if is_ajax(request):
           return JsonResponse(sendJson, status=200)

        return render(request, 'index.html')


def article_show(request):
    if(request.method == 'POST'):
        args = request.POST
        if("save" in args and "blank" in args and request.user.is_authenticated):
            user = User.objects.all()
            for i in user:
                if i.id == request.user.id:
                    i.email = args["blank"]
                    i.save()
        if("next" in args and "blank" in args and request.user.is_authenticated):
            return redirect('/next_stage')

    context = {"data": ""}
    if(request.user.is_authenticated):
        user = User.objects.all()
        for i in user:
            if i.id == request.user.id:
                context["data"] = i.email
                break

    return render(request, 'article_show.html', context)

def in_page(request):
    if(request.method == 'POST'):
        args = request.POST
        if("enter" in args and "code" in args):
            user = User.objects.all()
            for i in user:
                if i.password == args["code"]:
                    login(request, i)
                    return redirect('/')

    return render(request, 'in.html', {})

def next_stage(request):
    if(request.method == 'POST'):
        args = request.POST
        if("ready" in args and "blank" in args):
            now_article_title = ""
            new_user_created_id = request.user.id
            user = User.objects.all()
            for i in user:
                if i.id == new_user_created_id:
                    now_article_title = i.email
                    break
            new_article = ArticleFild()
            new_article.title = now_article_title
            new_article.user_created_id = new_user_created_id
            new_article.preview_title = args["blank"]
            new_article.save()
            return redirect('/')

    return render(request, 'next_stage.html', {})